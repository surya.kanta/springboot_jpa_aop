package com.example.pojo;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Employee {

		public int id;
		public String fname;
		public String lname;
		public String dob;
		public AddressReqVo address;
		public List<Vehicle> vehicles;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getFname() {
			return fname;
		}
		public void setFname(String fname) {
			this.fname = fname;
		}
		public String getLname() {
			return lname;
		}
		public void setLname(String lname) {
			this.lname = lname;
		}



	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return id == employee.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public Employee(int id, String fname, String lname, String dob, AddressReqVo address, List<Vehicle> vehicles) {
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.address = address;
		this.vehicles = vehicles;
	}

	public AddressReqVo getAddress() {
			return address;
		}
		public void setAddress(AddressReqVo address) {
			this.address = address;
		}
		@Override
		public String toString() {
			return "Employee [id=" + id + ", fname=" + fname + ", lname=" + lname + ", dob=" + dob + ", address="
					+ address + ", vehicles=" + vehicles + "]";
		}

	public Employee() {
	}

	public List<Vehicle> getVehicles() {
			return vehicles;
		}
		public void setVehicles(List<Vehicle> vehicles) {
			this.vehicles = vehicles;
		}


	public String getDob() {
		return dob;
	}
}
