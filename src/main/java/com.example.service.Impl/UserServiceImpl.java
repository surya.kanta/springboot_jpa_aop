package com.example.service.Impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import com.example.controller.DemoController;
import com.example.pojo.AddressReqVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.example.Entity.Address;
import com.example.Entity.Vehicle;

import com.example.pojo.Employee;
import com.example.repository.EmployeeRepository;
import com.example.util.CustomValidationException;
import com.example.service.UserService;

@Component
public class UserServiceImpl implements UserService{
	
	
	@Autowired
	public EmployeeRepository employeeRepository;
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public Employee finduser(Integer id) {
		// TODO Auto-generated method stub
		com.example.Entity.Employee e = employeeRepository.findOne(id);
		//employee
		Employee employee = new Employee() ;
		employee.setFname(e.getFname());
		employee.setLname(e.getLname());
		employee.setDob(new SimpleDateFormat("dd-MM-yyyy").format(e.getDob()));
		employee.setId(e.getId());
		//Address
		AddressReqVo addressReqVo = new AddressReqVo();
		addressReqVo.setDistrict(e.getAddress().getDistrict());
		addressReqVo.setId(e.getAddress().getId());
		addressReqVo.setPincode(e.getAddress().getPincode());
		addressReqVo.setState(e.getAddress().getState());
		employee.setAddress(addressReqVo);
		//vehicle

		e.getVehicleList().forEach(vehicle->{
			com.example.pojo.Vehicle v = new com.example.pojo.Vehicle();
			v.setId(vehicle.getId());
			v.setEmp_id(vehicle.getEmployee().getId());
			v.setRegistrationstate(vehicle.getRegistrationstate());
			v.setVehicleno(vehicle.getVehicleno());
			if(employee.getVehicles() == null){
				employee.setVehicles(new ArrayList<>(e.getVehicleList().size()));
			}
			employee.getVehicles().add(v);
		});
	return employee;
	}

	
	@Override
	public Employee createUser(Employee emp) throws CustomValidationException{
		// TODO Auto-generated method stub
		com.example.Entity.Employee e = new com.example.Entity.Employee();
		try {
		try {
			e.setDob(new SimpleDateFormat("dd-MM-yyyy").parse(emp.getDob()));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			
		}
		e.setFname(emp.getFname());
		e.setLname(emp.getLname());
		//setting address
		Address address = new Address();
		address.setDistrict(emp.getAddress().getDistrict());
		address.setPincode(emp.address.getPincode());
		address.setState(emp.getAddress().getState());
		e.setAddress(address);
		//vehicle
		emp.getVehicles().forEach(vehicle->{
			Vehicle v = new Vehicle();
			v.setRegistrationstate(vehicle.getRegistrationstate());
			v.setVehicleno(vehicle.getVehicleno());
			v.setEmployee(e);
			if(null == e.getVehicleList()) {
				e.setVehicleList(new ArrayList<Vehicle>());
			}
			e.getVehicleList().add(v);
		});
		
		com.example.Entity.Employee finalEmployee = employeeRepository.save(e);
		emp.setId(finalEmployee.getId());
		emp.getAddress().setId(finalEmployee.getAddress().getId());
		return emp;
		
		} catch (Exception ex) {
			// TODO: handle exception
			logger.error("error occured : {}", ex.getMessage());
			throw new CustomValidationException("503", ex.getMessage());
		}
	}

	@Override
	public Collection<com.example.pojo.Employee> finduserwithfname(String fname, String lname) throws CustomValidationException {
		// TODO Auto-generated method stub
		try {
		Collection<com.example.Entity.Employee> employeeCollection =  employeeRepository.findUserByFname(fname);
		Collection<Employee> employees = new ArrayList<>();
		employeeCollection.forEach(e -> {
			Employee employee = new Employee() ;
			employee.setFname(e.getFname());
			employee.setLname(e.getLname());
			employee.setDob(new SimpleDateFormat("dd-MM-yyyy").format(e.getDob()));
			employee.setId(e.getId());
			//Address
			AddressReqVo addressReqVo = new AddressReqVo();
			addressReqVo.setDistrict(e.getAddress().getDistrict());
			addressReqVo.setId(e.getAddress().getId());
			addressReqVo.setPincode(e.getAddress().getPincode());
			addressReqVo.setState(e.getAddress().getState());
			employee.setAddress(addressReqVo);
			//vehicle

			e.getVehicleList().forEach(vehicle->{
				com.example.pojo.Vehicle v = new com.example.pojo.Vehicle();
				v.setId(vehicle.getId());
				v.setEmp_id(vehicle.getEmployee().getId());
				v.setRegistrationstate(vehicle.getRegistrationstate());
				v.setVehicleno(vehicle.getVehicleno());
				if(employee.getVehicles() == null){
					employee.setVehicles(new ArrayList<>(e.getVehicleList().size()));
				}
				employee.getVehicles().add(v);
			});
			employees.add(employee);

		});

		return employees;
		} catch (Exception ex) {
			// TODO: handle exception
			logger.error("error occured : {}", ex.getMessage());
			throw new CustomValidationException("505", ex.getMessage());
		}
	}
	@Override
	public void deleteUser(Integer id) {
		// TODO Auto-generated method stub
		employeeRepository.delete(id);

	}
}
