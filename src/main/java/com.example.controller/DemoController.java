package com.example.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.customannotation.ApiAccessFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.example.pojo.Employee;
import com.example.util.CustomValidationException;
import com.example.service.UserService;

@Controller
@CrossOrigin
public class DemoController {

	@Autowired
	private UserService userService;

	Logger logger = LoggerFactory.getLogger(DemoController.class);


	 @RequestMapping(value = "welcome") public Object
	 welcomeUser(HttpServletRequest req, HttpServletResponse res) {
	 	try {
	 		return
	  "Welcome " + req.getParameter("name");
	 	} catch (Exception e) { // TODOhandle exception
	 		logger.error("error occured : {}", e.getMessage());
	 		return res; } }


	/*
	 * @RequestMapping(value = "welcomepage")
	 *
	 * @ResponseBody public ModelAndView welcomepage(HttpServletRequest req,
	 * HttpServletResponse res) { try { return new ModelAndView("index"); } catch
	 * (CustomValidationException e) { // TODO: handle exception
	 * logger.error("error occured : {}", e.getMessage()); return res; } catch
	 * (Exception e) { // TODO: handle exception logger.error("error occured : {}",
	 * e.getMessage()); return res; } }
	 */

	@ApiAccessFilter(isValidationNeeded = true,paramsToBeValidated = {"id"},headersToBevalidated = {""})
	@RequestMapping(value = "/finduser", method = RequestMethod.GET, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public Object findUser(HttpServletRequest req, HttpServletResponse res) {
		try {
		return userService.finduser(Integer.parseInt(req.getParameter("id")));
		} catch (CustomValidationException e) {
			// TODO: handle exception
			logger.error("error occured : {}", e.getMessage());
			return res;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("error occured : {}", e.getMessage());
			return res;
		}
	}

	@RequestMapping(value = "createuser", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public Object createUser(HttpServletRequest req, HttpServletResponse res, @RequestBody Employee emp) {
		try {
		return userService.createUser(emp);
		} catch (CustomValidationException e) {
			// TODO: handle exception
			logger.error("error occured : {}", e.getMessage());
			return res;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("error occured : {}", e.getMessage());
			return res;
		}
	}

	@RequestMapping(value = "finduserwithfname", method = RequestMethod.GET, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public Object finduserwithfname(HttpServletRequest req, HttpServletResponse res) {
		try {
			return userService.finduserwithfname(req.getParameter("fname"), req.getParameter("fname"));
		} catch (CustomValidationException e) {
			// TODO: handle exception
			logger.error("error occured : {}", e.getMessage());
			return res;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("error occured : {}", e.getMessage());
			return res;
		}

	}

	@DeleteMapping(value = "deleteUser", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public Object deleteUser(HttpServletRequest req, HttpServletResponse res,
							 @RequestParam("employeeId") String employeeId ){
		userService.deleteUser(Integer.parseInt(employeeId));
		return new Boolean(true);
	}
}
