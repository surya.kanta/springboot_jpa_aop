package com.example.Aspect;

import com.example.controller.DemoController;
import com.example.customannotation.ApiAccessFilter;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Arrays;

@Aspect
@Configuration
public class DemoControllerAspect {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Before("execution(* com.example.controller.*.*(..))")
    public void before(JoinPoint joinPoint){
        logger.error("finally here .. {}",joinPoint);
        System.err.println("in Aspect .......");

        for (Method method : joinPoint.getSignature().getDeclaringType().getMethods()) {
            if(joinPoint.getSignature().getName().equalsIgnoreCase(method.getName()) &&
                    method.isAnnotationPresent(ApiAccessFilter.class) &&
                    method.getAnnotation(ApiAccessFilter.class).isValidationNeeded()){
                String[] paramsToBeValidated = method.getAnnotation(ApiAccessFilter.class).paramsToBeValidated();
                //create object from validation factoryclass and validate params/headers
                Arrays.stream(paramsToBeValidated).forEach(e->{
                    logger.error("param validated :: {} ",e);
                });

            }
        }
    }
}
