package com.example.customannotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ApiAccessFilter {
    public static final String[] a = new String[]{};
    public boolean isValidationNeeded() default  false;
    public String[] paramsToBeValidated() default {""};
    public String[] headersToBevalidated() default {""};
}
