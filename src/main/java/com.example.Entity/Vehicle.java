package com.example.Entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "vehicle")
public class Vehicle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String vehicleno;
	
	private String registrationstate;

	@ManyToOne
	private Employee employee;

	public Vehicle(int id, String vehicleno, String registrationstate, Employee employee) {
		this.id = id;
		this.vehicleno = vehicleno;
		this.registrationstate = registrationstate;
		this.employee = employee;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}

	public void setRegistrationstate(String registrationstate) {
		this.registrationstate = registrationstate;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getId() {
		return id;
	}

	public String getVehicleno() {
		return vehicleno;
	}

	public String getRegistrationstate() {
		return registrationstate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public Vehicle() {
	}
}
